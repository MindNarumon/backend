const User = require('../models/User')
const userController = {
  userList: [
    { id: 1, name: 'Narumon', gender: 'F' },
    { id: 2, name: 'Hattaya', gender: 'F' }
  ],
  lastId: 3,
  async addUser  (req, res, next) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    console.log(payload)
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser  (req, res, next) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser  (req, res, next) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async  getUsers  (req, res, next) {
    // res.json(userController.getUsers())
    // pattern1
    // User.find({}).exec(function (err, users) {
    //   if (err) {
    //     res.status(500).send()
    //  }
    //  res.json(users)
    // })
    // pattern2
    // User.find({}).then(function (users) {
    //   res.json(users)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    // pattern3 Async Await
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser  (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
